const getSum = (str1, str2) => {

  if(typeof str1 !== 'string' || typeof str2 !== 'string'){
    return false;
  }

  if(str1 === ''){
      str1 = '0';
  }
  else if(str2 === ''){
      str2 = '0';
  }

  let isDigitF = /^\d+$/.test(str1);
  let isDigitS = /^\d+$/.test(str2);

  if(isDigitF === false || isDigitS === false){
    return false;
  }

  let res = "";

  str1 = str1.split("").reverse().join("");
  str2 = str2.split("").reverse().join("");
     
  let carry = 0;

  for(let i = 0; i < str1.length; i++)
  {
      let sum = ((str1[i].charCodeAt(0) - '0'.charCodeAt(0)) + (str2[i].charCodeAt(0) - '0'.charCodeAt(0)) + carry);
      res += String.fromCharCode(sum % 10 + '0'.charCodeAt(0)); 
      carry = Math.floor(sum / 10);
  }

  for(let i = str1.length; i < str2.length; i++)
  {
      let sum = ((str2[i].charCodeAt(0) - '0'.charCodeAt(0)) + carry);
      res += String.fromCharCode(sum % 10 + '0'.charCodeAt(0));
      carry = Math.floor(sum / 10);
  }
     
    if (carry > 0)
        res += String.fromCharCode(carry + '0'.charCodeAt(0));

     
    return res.split("").reverse().join("");


};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {

  let res = {'post' : 0 , 'сomments' : 0};

  for(var p of listOfPosts)
  {
      if(p.author === authorName){
        res['post'] += 1;
      }
      
      if(p.comments != null)
      {
        for(var com of p.comments.values())
        {
          if(com.author === authorName){
            res['сomments'] += 1;
        }
      }
    }   
  }

  return `Post:${res.post},comments:${res.сomments}`;
};

const tickets=(people)=> {
  
    let bills = [];
    const price = 25;
    let canGiveChange = false;
    

    for(let i = 0; i < people.length; i++)
    {
        if(i !== 0)
        {
            let diff = people[i] - price;

            for(let j = 0; j < bills.length; j++)
            {
                
              
                if(bills[j] == diff)
                {
                    bills.splice(j, 1);
                    bills.push(people[i]);
                    canGiveChange = true;
                    
                    break;
                }

              for(let k = j + 1; k < bills.length; k++)
              {
                  if(bills[j] + bills[k] == diff)
                  {
                      bills.splice(j, 1);
                      bills.splice(k, 1);
                      bills.push(people[i]);
                      canGiveChange = true;
                    
                      break;
                  }
                  
              }

              
            }

        }
        else
        {
          bills.push(people[i]);
        }
    }

    return canGiveChange ? 'YES' : 'NO';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
